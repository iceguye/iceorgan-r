//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use crate::utilities::{poly_to_array, dbfs_to_amp};
use crate::soundcontrol::MidiCouplerControl;
use serde::{Serialize, Deserialize};

#[derive(Debug, Copy, Clone)]
pub struct Instrument {
    pub attenuation_db: f64,
    pub pan: [f64; 128],
    pub tuning_semitones: f64,
    pub tuning_cents: [f64; 128],
    pub mod_lfo_freq_hz: f64,
    pub mod_lfo_pitch_c: f64,
    pub vib_lfo_freq_hz: f64,
    pub vib_lfo_pitch_c: f64,
    pub reverb: f64,
    pub attack: f64,
    pub release: f64,
}

impl Instrument {
    pub fn new (tuning_semitones: f64,
                tuning_cents_str: &str,
                vib_lfo_pitch_c: f64) -> Instrument {
        Instrument {
            attenuation_db: 18.0,
            pan: poly_to_array("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
            tuning_semitones: tuning_semitones,
            tuning_cents: poly_to_array(tuning_cents_str),
            mod_lfo_freq_hz: 6.871,
            mod_lfo_pitch_c: 0.0,
            vib_lfo_freq_hz: 3.440,
            vib_lfo_pitch_c: vib_lfo_pitch_c,
            reverb: 100.0,
            attack: 0.038,
            release: 0.333,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Instruments<'a> {
    pub names: [&'a str; 9],
    pub data: [Instrument; 9],
}

impl Instruments<'_> {
    pub fn new() -> Instruments<'static> {
        Instruments {
            names: ["Instrument1",
                    "Instrument2",
                    "Instrument3",
                    "Instrument4",
                    "Instrument5",
                    "Instrument6",
                    "Instrument7",
                    "Instrument8",
                    "Instrument9", ],
            data: [Instrument::new(-12.0,
                                   "!	2	6	0	8	0	7	8	8	6	7	7	3	1	3	4	3	5	8	1	4	0	3	1	4	4	3	7	7	3	3	1	2	7	3	1	1	3	4	6	6	7	3	6	1	3	0	2	7	2	6	6	4	1	1	1	7	6	6	4	7	6	0	7	4	1	3	5	3	4	4	8	5	7	6	6	6	3	8	1	0	2	5	3	3	6	1	5	1	3	3	7	6	6	5	2	5	3	1	8	6	5	1	5	3	2	4	2	1	5	1	4	5	0	6	0	1	5	8	4	0	7	0	0	7	4	7	1	1",
                                   -4.0),
                   Instrument::new(-7.0,
                                   "!	2	6	0	7	0	6	7	7	5	6	6	2	1	2	4	3	5	7	1	4	0	3	1	3	4	3	6	6	2	3	1	2	6	2	1	1	3	4	5	6	6	2	5	1	3	0	1	6	1	5	6	4	1	1	0	6	5	5	4	6	5	0	7	4	1	3	4	3	4	4	7	5	6	5	5	5	3	7	1	0	1	5	3	3	6	1	4	1	3	2	6	5	5	4	2	4	2	1	7	5	4	1	5	3	1	4	2	1	5	1	4	4	0	6	0	1	4	7	4	0	6	0	0	6	3	6	1	1",
                                   -3.0),
                   Instrument::new(0.0,
                                   "!	2	7	1	9	0	7	9	9	6	8	7	3	1	3	5	3	6	8	1	5	0	3	2	4	5	3	7	8	3	4	1	2	7	3	1	1	4	5	6	7	8	3	7	2	4	0	2	8	2	6	7	5	1	1	1	7	6	6	5	8	6	1	8	5	1	4	5	3	5	5	9	6	8	7	7	7	4	9	1	0	2	6	4	4	7	1	5	1	4	3	8	7	7	5	2	5	3	1	9	6	5	1	6	3	2	5	3	1	6	2	5	6	0	7	0	1	6	9	5	0	7	0	0	8	4	8	1	1",
                                   -2.0),
                   Instrument::new(12.0,
                                   "!	2	8	1	10	0	8	9	10	7	9	8	3	1	3	6	4	7	9	2	5	0	4	2	5	5	4	8	9	4	4	1	2	8	3	1	1	4	5	7	8	9	3	8	2	4	0	2	9	2	7	8	5	2	1	1	8	7	7	5	9	7	1	9	5	1	4	6	4	5	5	10	7	9	7	7	8	4	10	1	0	2	7	4	4	8	1	6	1	4	4	8	7	7	6	3	6	3	1	10	7	6	1	7	4	2	5	3	2	7	2	5	6	1	8	1	1	6	10	6	0	8	0	1	8	5	9	1	1",
                                   -1.0),
                   Instrument::new(19.0,
                                   "!	2	9	1	10	0	9	10	10	8	10	9	4	1	3	6	4	7	10	2	6	0	4	2	5	6	4	9	10	4	4	1	3	9	4	1	1	5	6	8	9	10	4	8	2	4	0	2	10	2	8	9	6	2	1	1	9	8	8	6	10	8	1	10	6	1	4	6	4	6	6	11	7	10	8	8	9	4	10	1	0	2	7	5	5	9	1	6	1	5	4	9	8	8	6	3	6	4	1	11	8	7	1	7	4	2	6	3	2	7	2	6	7	1	9	1	1	7	11	6	0	9	0	1	9	5	10	1	1",
                                   0.0),
                   Instrument::new(24.0,
                                   "!	3	10	1	11	0	10	11	11	8	11	10	4	1	4	7	5	8	11	2	6	1	5	2	6	6	5	10	11	4	5	1	3	10	4	1	1	5	6	9	9	11	4	9	2	5	0	2	10	2	8	10	6	2	1	1	10	9	9	6	11	9	1	11	6	1	5	7	5	6	6	12	8	11	9	9	9	5	11	1	0	3	8	5	5	10	2	7	2	5	4	10	9	9	7	3	7	4	1	12	9	7	2	8	5	2	6	4	2	8	2	6	7	1	9	1	1	8	12	7	0	10	0	1	10	6	11	1	1",
                                   1.0),
                   Instrument::new(28.0,
                                   "!	3	10	1	12	0	11	12	12	9	12	11	4	1	4	7	5	9	12	2	7	1	5	2	6	7	5	11	12	5	5	1	3	11	4	1	1	5	7	9	10	12	4	10	2	5	0	2	11	3	9	10	7	2	1	1	11	9	9	7	12	9	1	12	7	1	5	8	5	7	7	13	9	12	10	10	10	5	12	1	0	3	9	5	6	10	2	8	2	6	5	11	10	9	7	3	8	4	2	13	9	8	2	8	5	3	7	4	2	9	2	7	8	1	10	1	1	8	13	7	0	11	0	1	11	6	12	1	1",
                                   2.0),
                   Instrument::new(31.0,
                                   "!	3	11	1	13	0	12	13	13	10	13	11	5	2	4	8	5	9	13	2	7	1	5	3	7	7	5	12	13	5	6	1	3	12	5	2	1	6	7	10	11	13	5	11	2	6	0	3	12	3	10	11	7	2	1	1	12	10	10	8	13	10	1	13	7	1	6	8	5	7	7	14	9	13	10	10	11	5	13	1	0	3	9	6	6	11	2	8	2	6	5	12	10	10	8	4	8	5	2	14	10	8	2	9	5	3	7	4	2	9	2	8	9	1	11	1	1	9	14	8	0	12	0	1	12	7	13	1	1",
                                   3.0),
                   Instrument::new(36.0,
                                   "!	3	12	1	14	0	12	14	14	11	13	12	5	2	5	8	6	10	14	2	8	1	6	3	7	8	6	12	14	5	6	1	4	12	5	2	1	6	8	11	12	14	5	12	3	6	0	3	13	3	10	12	8	2	1	1	12	11	11	8	13	11	1	14	8	1	6	9	6	8	8	14	10	14	11	11	12	6	14	2	0	3	10	6	6	12	2	9	2	7	5	13	11	11	8	4	9	5	2	15	11	9	2	10	6	3	8	5	2	10	3	8	9	1	12	1	1	10	15	8	0	12	0	1	13	7	14	1	1",
                                   4.0),]
        }
    }

    pub fn get_data (&self, search: &str) -> Result<Instrument, &str> {
        let names = self.names;
        let mut result: Result<Instrument, &str>
            = Err("Can't find any data under this name.");
        let mut wc = 0;
        while wc < names.len() {
            if names[wc] == search {
                result = Ok(self.data[wc]);
                break;
            }
            wc = wc + 1;
        }
        result
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Stop<'a> {
    pub name: &'a str,
    pub is_out: bool,
    pub instruments_db: [(&'a str, Option<f64>); 9],
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Stops<'a> {
    pub great: [Stop<'a>; 14],
    pub swell: [Stop<'a>; 11],
    pub pedal: [Stop<'a>; 10],
}

impl Stops<'_> {
    pub fn new() -> Stops<'static>{
        Stops {
            great: [
                Stop {
                    name: "Trumpet8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(10.0)),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", Some(5.0)),
                        ("Instrument7", Some(5.0)),
                        ("Instrument8", Some(10.0)),
                        ("Instrument9", Some(35.0)),
                    ],
                },
                Stop {
                    name: "Mixture5",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", Some(5.0)),
                        ("Instrument7", Some(15.0)),
                        ("Instrument8", Some(20.0)),
                        ("Instrument9", Some(10.0)),
                    ],
                },
                Stop {
                    name: "Cornet2&4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", None),
                        ("Instrument6", Some(0.0)),
                        ("Instrument7", Some(5.0)),
                        ("Instrument8", Some(10.0)),
                        ("Instrument9", Some(25.0)),
                    ],
                },
                Stop {
                    name: "Raushquinte2_2-3",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", None),
                        ("Instrument5", Some(-5.0)),
                        ("Instrument6", Some(10.0)),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", Some(10.0)),
                    ],
                },
                Stop {
                    name: "Piccolo2'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", None),
                        ("Instrument5", None),
                        ("Instrument6", Some(0.0)),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", Some(15.0)),
                    ],
                },
                Stop {
                    name: "Octave4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(-5.0)),
                        ("Instrument5", None),
                        ("Instrument6", Some(10.0)),
                        ("Instrument7", None),
                        ("Instrument8", Some(30.0)),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "HohlFlute4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", None),
                        ("Instrument6", Some(5.0)),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "HarmonicFlute8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(10.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Rohrflote8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(25.0)),
                        ("Instrument5", Some(20.0)),
                        ("Instrument6", None),
                        ("Instrument7", Some(30.0)),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Salicional8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(30.0)),
                        ("Instrument4", Some(20.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(25.0)),
                        ("Instrument8", Some(30.0)),
                        ("Instrument9", Some(35.0)),
                    ],
                },
                Stop {
                    name: "Gamba8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(25.0)),
                        ("Instrument4", Some(20.0)),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(20.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", Some(35.0)),
                    ],
                },
                Stop {
                    name: "Prinzipal8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(25.0)),
                        ("Instrument4", Some(20.0)),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(20.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", Some(35.0)),
                    ],
                },
                Stop {
                    name: "Bourdon16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(0.0)),
                        ("Instrument2", Some(35.0)),
                        ("Instrument3", None),
                        ("Instrument4", None),
                        ("Instrument5", None),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Prinzipal16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(5.0)),
                        ("Instrument2", Some(35.0)),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(25.0)),
                        ("Instrument8", Some(30.0)),
                        ("Instrument9", None),
                    ],
                },
            ],
            swell: [
                Stop {
                    name: "Lieblgedekt 16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(-10.0)),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", None),
                        ("Instrument5", None),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "GeigenPrinzipl16",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", Some(35.0)),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(25.0)),
                        ("Instrument8", Some(30.0)),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Gedekt 8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(-10.0)),
                        ("Instrument4", Some(10.0)),
                        ("Instrument5", None),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Concert Flute 8",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(-10.0)),
                        ("Instrument4", None),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Gemshorn 8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(15.0)),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", Some(25.0)),
                        ("Instrument7", Some(20.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", Some(30.0)),
                    ],
                },
                Stop {
                    name: "Chalumeau 8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(10.0)),
                        ("Instrument4", Some(25.0)),
                        ("Instrument5", Some(10.0)),
                        ("Instrument6", Some(20.0)),
                        ("Instrument7", Some(15.0)),
                        ("Instrument8", Some(20.0)),
                        ("Instrument9", Some(25.0)),
                    ],
                },
                Stop {
                    name: "Aeoline 8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(25.0)),
                        ("Instrument4", Some(15.0)),
                        ("Instrument5", Some(20.0)),
                        ("Instrument6", Some(25.0)),
                        ("Instrument7", Some(25.0)),
                        ("Instrument8", None),
                        ("Instrument9", Some(25.0)),
                    ],
                },
                Stop {
                    name: "VoixCeleste 8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(30.0)),
                        ("Instrument4", Some(15.0)),
                        ("Instrument5", Some(10.0)),
                        ("Instrument6", Some(15.0)),
                        ("Instrument7", Some(30.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", Some(25.0)),
                    ],
                },
                Stop {
                    name: "Fugara 4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", None),
                        ("Instrument6", Some(0.0)),
                        ("Instrument7", None),
                        ("Instrument8", Some(5.0)),
                        ("Instrument9", Some(10.0)),
                    ],
                },
                Stop {
                    name: "Travesflute 4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(-5.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },
                Stop {
                    name: "Progressio 2&4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", Some(0.0)),
                        ("Instrument3", None),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(0.0)),
                        ("Instrument6", Some(0.0)),
                        ("Instrument7", None),
                        ("Instrument8", Some(0.0)),
                        ("Instrument9", None),
                    ],
                },
            ],
            pedal: [
                Stop {
                    name: "Posaune16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(-5.0)),
                        ("Instrument2", Some(-10.0)),
                        ("Instrument3", Some(-10.0)),
                        ("Instrument4", Some(-5.0)),
                        ("Instrument5", Some(25.0)),
                        ("Instrument6", Some(10.0)),
                        ("Instrument7", Some(15.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "PrinzipalBass16",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(0.0)),
                        ("Instrument2", Some(30.0)),
                        ("Instrument3", Some(0.0)),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(10.0)),
                        ("Instrument6", Some(15.0)),
                        ("Instrument7", Some(20.0)),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "BassViolin16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(-5.0)),
                        ("Instrument2", Some(10.0)),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(10.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", Some(25.0)),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "SubBass16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(-5.0)),
                        ("Instrument2", None),
                        ("Instrument3", Some(-5.0)),
                        ("Instrument4", None),
                        ("Instrument5", None),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "EchoBass16'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", Some(5.0)),
                        ("Instrument2", None),
                        ("Instrument3", Some(5.0)),
                        ("Instrument4", Some(15.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "Quintbass10_2-3",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", Some(-10.0)),
                        ("Instrument3", None),
                        ("Instrument4", None),
                        ("Instrument5", None),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "OctaveBass8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(-10.0)),
                        ("Instrument4", Some(5.0)),
                        ("Instrument5", Some(25.0)),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "Violoncello8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(20.0)),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(5.0)),
                        ("Instrument6", Some(0.0)),
                        ("Instrument7", Some(10.0)),
                        ("Instrument8", Some(15.0)),
                        ("Instrument9", Some(25.0)),
                    ],
                },                
                Stop {
                    name: "BassFlute8'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", Some(-5.0)),
                        ("Instrument4", Some(0.0)),
                        ("Instrument5", Some(15.0)),
                        ("Instrument6", None),
                        ("Instrument7", None),
                        ("Instrument8", None),
                        ("Instrument9", None),
                    ],
                },                
                Stop {
                    name: "OctaveBass4'",
                    is_out: false,
                    instruments_db: [
                        ("Instrument1", None),
                        ("Instrument2", None),
                        ("Instrument3", None),
                        ("Instrument4", Some(-10.0)),
                        ("Instrument5", None),
                        ("Instrument6", Some(5.0)),
                        ("Instrument7", None),
                        ("Instrument8", Some(25.0)),
                        ("Instrument9", None),
                    ],
                },                
            ],
        }
    }

    pub fn get_amps (&self, express: u64) -> [[i32; 9]; 3] {
        let expressf = express as f64;
        let express_norm = expressf / 127.0;
        let mut amps: [[i32; 9]; 3] = [[0; 9]; 3];
        let mut ampsf: [[f64; 9]; 3] = [[0.0; 9]; 3];
        let mut i = 0;
        while i < amps.len() {
            if i == 0 {
                let mut k = 0;
                while k < amps[i].len() {
                let mut j = 0;
                    while j < self.great.len() {
                        let is_out = self.great[j].is_out;
                        let (_inst_name, inst_db)
                            = self.great[j].instruments_db[k];
                        if inst_db != None && is_out == true {
                            let ampf: f64 = dbfs_to_amp(-inst_db.unwrap());
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32; 
                        } else {
                            let ampf: f64 = 0.0;
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32; 
                        }
                        j = j + 1;
                    }
                    k = k + 1;
                }
            }
            if i == 1 {
                let mut k = 0;
                while k < amps[i].len() {
                let mut j = 0;
                    while j < self.swell.len() {
                        let is_out = self.swell[j].is_out;
                        let (_inst_name, inst_db)
                            = self.swell[j].instruments_db[k];
                        if inst_db != None && is_out == true {
                            let ampf: f64 = dbfs_to_amp(-inst_db.unwrap());
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32; 
                        } else {
                            let ampf: f64 = 0.0;
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32;
                        }
                        j = j + 1;
                    }
                    k = k + 1;
                }
            }
            if i == 2 {
                let mut k = 0;
                while k < amps[i].len() {
                let mut j = 0;
                    while j < self.pedal.len() {
                        let is_out = self.pedal[j].is_out;
                        let (_inst_name, inst_db)
                            = self.pedal[j].instruments_db[k];
                        if inst_db != None && is_out == true {
                            let ampf: f64 = dbfs_to_amp(-inst_db.unwrap());
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32; 
                        } else {
                            let ampf: f64 = 0.0;
                            ampsf[i][k]
                                = (ampsf[i][k].powf(2.0) + ampf.powf(2.0))
                                .sqrt();
                            amps[i][k] = (ampsf[i][k] * 32.0 * express_norm)
                                as i32;
                        }
                        j = j + 1;
                    }
                    k = k + 1;
                }
            }
            i = i + 1;
        }
        amps
    }

    pub fn set_stop(&mut self, keyboard: &str, stop: &str, is_out: bool){
        if keyboard == "Great" {
            let mut wc = 0;
            while wc < self.great.len() {
                if self.great[wc].name == stop{
                    self.great[wc].is_out = is_out;
                    break;
                }
                wc = wc + 1;
            }
        }
        if keyboard == "Swell" {
            let mut wc = 0;
            while wc < self.swell.len() {
                if self.swell[wc].name == stop{
                    self.swell[wc].is_out = is_out;
                    break;
                }
                wc = wc + 1;
            }
        }
        if keyboard == "Pedal" {
            let mut wc = 0;
            while wc < self.pedal.len() {
                if self.pedal[wc].name == stop{
                    self.pedal[wc].is_out = is_out;
                    break;
                }
                wc = wc + 1;
            }
        }
    }
    pub fn regs_to_stops(&mut self,
                         regs: Registrations) {
        let mut i = 0;
        while i < self.great.len() {
            self.great[i].is_out = regs.regs_great[regs.working_reg_great][i];
            i = i + 1;
        }
        let mut i = 0;
        while i < self.swell.len() {
            self.swell[i].is_out = regs.regs_swell[regs.working_reg_swell][i];
            i = i + 1;
        }
        let mut i = 0;
        while i < self.pedal.len() {
            self.pedal[i].is_out = regs.regs_pedal[regs.working_reg_pedal][i];
            i = i + 1;
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Deserialize, Serialize)]
pub struct Registrations {
    pub regs_great: [[bool; 14]; 10],
    pub regs_swell: [[bool; 11]; 10],
    pub regs_pedal: [[bool; 10]; 10],
    pub working_reg_great: usize,
    pub working_reg_swell: usize,
    pub working_reg_pedal: usize,
    pub swell_great: bool,
    pub swell_pedal: bool,
    pub great_pedal: bool,
}

impl Registrations {
    pub fn new () -> Registrations {
        Registrations {
            regs_great: [[false; 14]; 10],
            regs_swell: [[false; 11]; 10],
            regs_pedal: [[false; 10]; 10],
            working_reg_great: 0,
            working_reg_swell: 0,
            working_reg_pedal: 0,
            swell_great: false,
            swell_pedal: false,
            great_pedal: false,
        }
    }

    pub fn activate_reg(&mut self, keyboard: String, acti_num: usize) {
        if keyboard == "Great".to_string() {
            self.working_reg_great = acti_num;
        } else if keyboard == "Swell".to_string() {
            self.working_reg_swell = acti_num;
        } else {
            self.working_reg_pedal = acti_num;
        }
    }
    
    pub fn stops_to_regs(&mut self,
                         stops: Stops) {
        let mut i = 0;
        while i < stops.great.len() {
            let stop = stops.great[i];
            self.regs_great[self.working_reg_great][i] = stop.is_out;  
            i = i + 1;
        }
        let mut i = 0;
        while i < stops.swell.len() {
            let stop = stops.swell[i];
            self.regs_swell[self.working_reg_swell][i] = stop.is_out;
            i = i + 1;
        }
        let mut i = 0;
        while i < stops.pedal.len() {
            let stop = stops.pedal[i];
            self.regs_pedal[self.working_reg_pedal][i] = stop.is_out;
            i = i + 1;
        }
    }

    pub fn couplers_to_regs(&mut self, midi_coupler: MidiCouplerControl) {
        self.swell_great = midi_coupler.swell_great;
        self.swell_pedal = midi_coupler.swell_pedal;
        self.great_pedal = midi_coupler.great_pedal;
    }
}
