//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use sdl2::mixer::Chunk;
use sdl2::mixer::Channel;
use std::{thread, time};
use crate::iceorgandata::Registrations;

pub struct Sounds {
    pub chunks: [[[Option<Chunk>; 128]; 9]; 3],
    pub chiff: Option<Chunk>,
    pub noise_breath_start: Option<Chunk>,
    pub noise_breath_loop: Option<Chunk>,
    pub hall_chunks: [Option<Chunk>; 128],
}

impl Sounds {
    pub fn new() -> Sounds {
        const INIT_CHUNK: Option<Chunk> = None;
        const INIT_CHUNKS_KB: [Option<Chunk>; 128] = [INIT_CHUNK; 128];
        const INIT_CHUNKS_ALL: [[Option<Chunk>; 128]; 9]
            = [INIT_CHUNKS_KB; 9];
        const INIT_HALL_CHUNK: Option<Chunk> = None;
        let mut chunks: [[[Option<Chunk>; 128]; 9]; 3] = [INIT_CHUNKS_ALL; 3];
        let mut hall_chunks: [Option<Chunk>; 128] = [INIT_HALL_CHUNK; 128];
        let mut kb = 0;
        while kb < 3 { 
            let mut inst = 0;
            while inst < 9 {
                let mut note = 0;
                while note < 128 {
                    let inst_str = (inst + 1).to_string();
                    let inst_str = inst_str.as_str();
                    let inst_name = "Instrument".to_string() + inst_str;
                    let inst_name = inst_name.as_str();
                    let note_str = note.to_string();
                    let note_str = note_str.as_str();
                    let sinepath = String::from("data/generated-samples/")
                        + inst_name + "/sine-" + note_str + ".wav";
                    let sinepath = sinepath.as_str();
                    let sound = Chunk::from_file(sinepath).unwrap();
                    chunks[kb][inst][note] = Some(sound);
                    note = note + 1;
                }
                inst = inst + 1;
            }
            kb = kb + 1;
        }
        Sounds {
            chunks: chunks,
            chiff: Some(Chunk::from_file(
                "data/generated-samples/effects/Chiff-use.wav")
                        .unwrap()),
            noise_breath_start: Some(Chunk::from_file(
                "data/generated-samples/effects/noise_breath-start-use.wav")
                                     .unwrap()),
            noise_breath_loop: Some(Chunk::from_file(
                "data/generated-samples/effects/noise_breath-loop-use.wav")
                                    .unwrap()),
            hall_chunks: hall_chunks,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct MidiCouplerControl<'a> {
    pub input_names: [&'a str; 3],
    pub midi_polls: [bool; 3],
    pub new_inputs: [[u64; 4]; 3],
    pub actual_inputs: [[u64; 4]; 3],
    pub swell_great: bool,
    pub swell_pedal: bool,
    pub great_pedal: bool,
    pub parent_keys_down: [[bool; 128]; 3],
    pub keys_down: [[bool; 128]; 3],
    pub channels: [[[i32; 128]; 9]; 3],
    pub is_playing: [[[bool; 128]; 9]; 3],
}

impl MidiCouplerControl<'_> {
    pub fn new() -> MidiCouplerControl<'static> {
        let mut channels: [[[i32; 128]; 9]; 3] = [[[0; 128]; 9]; 3];
        let is_playing: [[[bool; 128]; 9]; 3] = [[[false; 128]; 9]; 3];
        let mut channel: i32 = 0;
        let mut kb = 0;
        while kb < 3 { 
            let mut inst = 0;
            while inst < 9 {
                let mut note = 0;
                while note < 128 {
                    channels[kb][inst][note] = channel;
                    channel = channel + 1;
                    note = note + 1;
                }
                inst = inst + 1;
            }
            kb = kb + 1;
        }
        MidiCouplerControl {
            input_names: ["Great", "Swell", "Pedal"],
            midi_polls: [false, false, false, ],
            new_inputs: [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            actual_inputs: [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            swell_great: false,
            swell_pedal: false,
            great_pedal: false,
            parent_keys_down: [[false; 128]; 3],
            keys_down: [[false; 128]; 3],
            channels: channels,
            is_playing: is_playing,
        }
    }

    pub fn get_coupler(&self, coupler: &str) -> Result<bool, &str>{
        let mut result: Result<bool, &str>
            = Err("Input an invalid coupler");
        if coupler == "Swell-Great" {
            result = Ok(self.swell_great);
        }
        if coupler == "Swell-Pedal" {
            result = Ok(self.swell_pedal);
        }
        if coupler == "Great-Pedal" {
            result = Ok(self.great_pedal);
        }
        result
    }
    
    pub fn set_coupler(&mut self, coupler: &str, is_out: bool) {
        if coupler == "Swell-Great" {
            self.swell_great = is_out;
            let mut i = 0;
            while i < self.keys_down[1].len() {
                if is_out == true {
                    if self.keys_down[1][i] == false {
                        self.keys_down[1][i] = self.parent_keys_down[0][i];
                    }
                } else {
                    self.keys_down[1][i] = self.parent_keys_down[1][i];
                }
                i = i + 1;
            }
        }
        if coupler == "Swell-Pedal" {
            self.swell_pedal = is_out;
            let mut i = 0;
            while i < self.keys_down[1].len() {
                if is_out == true {
                    if self.keys_down[1][i] == false {
                        self.keys_down[1][i] = self.parent_keys_down[2][i];
                    }
                } else {
                    self.keys_down[1][i] = self.parent_keys_down[1][i];
                }
                i = i + 1;
            }
        }
        if coupler == "Great-Pedal" {
            self.great_pedal = is_out;
            let mut i = 0;
            while i < self.keys_down[0].len() {
                if is_out == true {
                    if self.keys_down[0][i] == false {
                        self.keys_down[0][i] = self.parent_keys_down[2][i];
                    }
                } else {
                    self.keys_down[0][i] = self.parent_keys_down[0][i];
                }
                i = i + 1;
            }
        }
    }

    pub fn set_keys_down(&mut self, midi_channels: [u64; 3]) {
        let actual_inputs = self.actual_inputs;
        let mut i = 0;
        while i < actual_inputs.len() {
            let is_key_down = actual_inputs[i][3] > 0
                && (actual_inputs[i][1] == 143 + midi_channels[i]);
            let is_key_up = (actual_inputs[i][3] == 0
                             && (actual_inputs[i][1] == 143 + midi_channels[i]))
                || (actual_inputs[i][1] == 127 + midi_channels[i]);
            let note = actual_inputs[i][2] as usize;
            if is_key_down {
                self.parent_keys_down[i][note] = true;
                self.keys_down[i][note] = self.parent_keys_down[i][note];
            } else if is_key_up {
                self.parent_keys_down[i][note] = false;
                if i == 1 {
                    if self.swell_great {
                        if self.parent_keys_down[0][note] == false {
                            self.keys_down[i][note]
                                = self.parent_keys_down[i][note];
                        }
                    } else if self.swell_pedal {
                        if self.parent_keys_down[2][note] == false {
                            self.keys_down[i][note]
                                = self.parent_keys_down[i][note];
                        }
                    } else {
                        self.keys_down[i][note]
                            = self.parent_keys_down[i][note];
                    }
                }
                if i == 0 {
                    if self.great_pedal {
                        if self.parent_keys_down[2][note] == false {
                            self.keys_down[i][note]
                                = self.parent_keys_down[i][note];
                        }
                    } else {
                        self.keys_down[i][note]
                            = self.parent_keys_down[i][note];
                    }
                }
            }

            if i == 0 && self.swell_great == true {
                self.keys_down[1][note]
                    = self.parent_keys_down[i][note];
            }
            if i == 2 && self.swell_pedal == true {
                self.keys_down[1][note]
                    = self.parent_keys_down[i][note];
            }                
            if i == 2 && self.great_pedal == true {
                self.keys_down[0][note]
                    = self.parent_keys_down[i][note];
            }                
            i = i + 1;
        }
    }

    pub fn set_sounds(&mut self,
                      chunks: &[[[Option<Chunk>; 128]; 9]; 3],
                      vols: [[i32; 9]; 3]) {
        let mut i = 0;
        while i < self.keys_down.len() {
            let mut j = 0;
            while j < self.channels[i].len() {
                let mut k = 0;
                while k < self.keys_down[i].len() {
                    let is_down = self.keys_down[i][k];
                    let is_playing = self.is_playing[i][j][k];
                    if is_down == true && is_playing == false {
                        let chunk = chunks[i][j][k].as_ref().unwrap();
                        let channel = self.channels[i][j][k];
                        Channel(channel).set_volume(vols[i][j]);
                        Channel(channel).fade_in(&chunk, -1, 38).unwrap();
                        self.is_playing[i][j][k] = true;
                    }
                    if is_down == false && is_playing == true {
                        let channel = self.channels[i][j][k];
                        Channel(channel).fade_out(333);
                        self.is_playing[i][j][k] = false;
                    }
                    k = k + 1;
                }
                j = j + 1;
            }
            i = i + 1;
        }
    }

    pub fn custom_fade(channel: i32, old_vol: i32, new_vol: i32) {
        if new_vol > old_vol {
            let ms = 38;
            let delta_vol = new_vol - old_vol;
            let rest_ms = ms / delta_vol;
            let rest_time = time::Duration::from_millis(rest_ms as u64);
            let mut i = old_vol;
            while i <  new_vol{
                Channel(channel).set_volume(i);
                thread::sleep(rest_time);
                i = i + 1;
            }
        } else {
            let ms = 333;
            let delta_vol = old_vol - new_vol;
            let rest_ms = ms / delta_vol;
            let rest_time = time::Duration::from_millis(rest_ms as u64);
            let mut i = old_vol;
            while i >  new_vol{
                Channel(channel).set_volume(i);
                thread::sleep(rest_time);
                i = i - 1;
            }
        }
    }

    pub fn stops_set_sounds(&self,
                            vols: [[i32; 9]; 3]) {
        let mut i = 0;
        while i < self.keys_down.len() {
            let mut j = 0;
            while j < self.channels[i].len() {
                let mut k = 0;
                while k < self.keys_down[i].len() {
                    let is_playing = self.is_playing[i][j][k];
                    if is_playing == true {
                        let channel = self.channels[i][j][k];
                        let old_vol = Channel(channel).get_volume();
                        let new_vol = vols[i][j];
                        thread::spawn (move || {
                            MidiCouplerControl::<'_>::custom_fade
                                (channel, old_vol, new_vol);
                        });
                    }
                    k = k + 1;
                }
                j = j + 1;
            }
            i = i + 1;
        }
    }
    
    pub fn set_express(&self,
                       vols: [[i32; 9]; 3]) {
        let mut i = 0;
        while i < self.channels.len() {
            let mut j = 0;
            while j < self.channels[i].len() {
                let mut k = 0;
                while k < self.channels[i][j].len() {
                    let channel = self.channels[i][j][k];
                    Channel(channel).set_volume(vols[i][j]);
                    k = k + 1;
                }
                j = j + 1;
            }
            i = i + 1;
        }
    }

    pub fn regs_to_couplers(&mut self, regs: Registrations) {
        self.swell_great = regs.swell_great;
        self.swell_pedal = regs.swell_pedal;
        self.great_pedal = regs.great_pedal;
    }
}
