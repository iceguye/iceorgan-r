//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

mod iceorgandata;
mod preparesamples;
mod soundcontrol;
mod utilities;
mod midi_input;
mod window;

use std::env;
use std::process::exit;
use std::path::Path;
use std::fs::{File};
use std::io::{Read, Write};
use std::thread;
use std::sync::{Arc, Mutex};
use sdl2::mixer;
use sdl2::mixer::AUDIO_F32LSB;
use crate::preparesamples::preparesamples;
use crate::iceorgandata::{Stops, Registrations};
use crate::soundcontrol::{Sounds,
                          MidiCouplerControl};
use crate::utilities::{dist_clean};
use crate::midi_input::MidiRunner;
use crate::window::{display_window, MouseSignal};

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut has_init: bool = Path::new("data/generated-samples").is_dir();
    let mut do_dist_clean: bool = false;
    let mut arg_cnt = 0;
    while arg_cnt <  args.len() {
        if args[arg_cnt].as_str() == "--dist-clean" {
            do_dist_clean = true;
        } else if arg_cnt > 0 {
            println!("Error: {} is an invalid argument.",
                     args[arg_cnt].as_str());
            std::process::abort();
        }
        arg_cnt = arg_cnt + 1;
    }
    if do_dist_clean == true {
        dist_clean();
        has_init = false;
    } else if has_init == false {
        preparesamples();
        has_init = true;
    }
    if has_init == true {
        
        // Initiate the SDL2 Mixer.
        mixer::open_audio(96000, AUDIO_F32LSB, 2, 1024).unwrap();
        mixer::allocate_channels(5000);
        
        // Set the 3 required midi channels below. They are:
        // [Great, Swell, Pedal]
        let mut midi_channels = [1, 1, 1];
        
        // Start the sounds, stops, and midi coupler controller.
        let mut stops = Stops::new();
        let sounds = Sounds::new();
        let mut midi_coupler = MidiCouplerControl::new();

        // Initiate registrations, and try to load the saved
        // registrations if toml exsit.
        let mut regs: Registrations;
        let is_saved: bool =  Path::new("Registrations.toml").is_file();
        if is_saved {
            let mut regs_file = File::open("Registrations.toml").unwrap();
            let mut regs_string = String::new();
            regs_file.read_to_string(&mut regs_string).unwrap();
            let regs_str = regs_string.as_str();
            regs = toml::from_str(regs_str).unwrap();
            stops.regs_to_stops(regs);
            midi_coupler.regs_to_couplers(regs);
        } else {
            regs = Registrations::new();
        }
        
        // Request user to select Midi input devices. 3 devices for
        // Great, Swell, and Pedal keyboards are required to run this
        // program. Virtual keyboards are acceptable for testing
        // purpose.
        println!(
"

Select Midi input ports. 3 input ports for Great, Swell, and Pedal
keyboards are required to run this program. Virtual keyboards are
acceptable for testing purpose.
");
        println!("Select an input port for Great manual: ");
        let mut midi_great = MidiRunner::new();
        match midi_great.input_select() {
            Ok(_) => (),
            Err(err) => {
                println!("Error: {}", err);
                exit(1);         
            }
        }
        println!("Select an input port for Swell manual: ");
        let mut midi_swell = MidiRunner::new();
        match midi_swell.input_select() {
            Ok(_) => (),
            Err(err) => {
                println!("Error: {}", err);
                exit(1);         
            }
        }
        println!("Select an input port for Pedal: ");
        let mut midi_pedal = MidiRunner::new();
        match midi_pedal.input_select() {
            Ok(_) => (),
            Err(err) => {
                println!("Error: {}", err);
                exit(1);         
            }
        }
        
        // Spawn threads to run the midi reader, and share memory with
        // the mean loop.
        let midi_great_rcv = Arc::new(Mutex::new(
            [0, 143 + midi_channels[0], 0, 0]));
        let midi_great_snd = Arc::clone(&midi_great_rcv);
        let midi_swell_rcv = Arc::new(Mutex::new(
            [0, 143 + midi_channels[1], 0, 0]));
        let midi_swell_snd = Arc::clone(&midi_swell_rcv);
        let midi_pedal_rcv = Arc::new(Mutex::new(
            [0, 143 + midi_channels[2], 0, 0]));
        let midi_pedal_snd = Arc::clone(&midi_pedal_rcv);
        thread::spawn (move || {
            match midi_great.run_reader(midi_great_snd) {
                Ok(_) => (),
                Err(err) => println!("Error: {}", err)
            }
        });
        thread::spawn (move || {
            match midi_swell.run_reader(midi_swell_snd) {
                Ok(_) => (),
                Err(err) => println!("Error: {}", err)
            }
        });
        thread::spawn (move || {
            match midi_pedal.run_reader(midi_pedal_snd) {
                Ok(_) => (),
                Err(err) => println!("Error: {}", err)
            }
        });

        // Define some mutable variables for the main loop.
        let mut express: u64 = 127;
        let mut old_mouse_signal = MouseSignal
        {
            set_stop: ("".to_string(), "".to_string(), false),
            set_coupler: ("".to_string(), false),
            set_reg: ("".to_string(), 0),
        };

        // Run the graphic interface.
        let exit_rcv = Arc::new(Mutex::new(false));
        let exit_snd = Arc::clone(&exit_rcv);
        let mouse_signal_rcv = Arc::new(Mutex::new(old_mouse_signal.clone()));
        let mouse_signal_snd = Arc::clone(&mouse_signal_rcv);
        let keys_down_rcv = Arc::new(Mutex::new(midi_coupler.keys_down));
        let keys_down_snd = Arc::clone(&keys_down_rcv);
        let midi_channels_rcv = Arc::new(Mutex::new(midi_channels));
        let midi_channels_snd = Arc::clone(&midi_channels_rcv);
        thread::spawn (move || {
            display_window(exit_snd,
                           stops.clone(),
                           regs.clone(),
                           midi_coupler.clone(),
                           mouse_signal_snd,
                           keys_down_rcv,
                           midi_channels_snd);
        });
        
        // Start the main loop here.
        loop {
            let should_exit = *exit_rcv.lock().unwrap();
            if should_exit == true {
                // Saved the registrations to toml before exit.
                let regs_toml_string = toml::to_string(&regs).unwrap();
                let mut regs_toml_file = File::create("Registrations.toml")
                    .unwrap();
                let regs_tome_str = regs_toml_string.as_str();
                write!(regs_toml_file, "{}", regs_tome_str).unwrap();
                break;
            }
            midi_channels = *midi_channels_rcv.lock().unwrap();
            let mouse_signal = mouse_signal_rcv.lock().unwrap().clone();
            if mouse_signal.set_stop != old_mouse_signal.set_stop {
                old_mouse_signal.set_stop = mouse_signal.set_stop.clone();
                let (keyboard, stop, is_out) = &mouse_signal.set_stop;
                let keyboard = keyboard.as_str();
                let stop = stop.as_str();
                stops.set_stop(keyboard, stop, *is_out);
                let vols = stops.get_amps(express);
                midi_coupler.stops_set_sounds(vols);
                regs.stops_to_regs(stops);
            }
            if mouse_signal.set_coupler != old_mouse_signal.set_coupler {
                old_mouse_signal.set_coupler = mouse_signal.set_coupler.clone();
                let (coupler, is_out) = &mouse_signal.set_coupler;
                let coupler = coupler.as_str();
                midi_coupler.set_coupler(coupler, *is_out);
                let vols = stops.get_amps(express);
                midi_coupler.set_sounds(&sounds.chunks,
                                        vols);
                regs.couplers_to_regs(midi_coupler);
            }
            if mouse_signal.set_reg != old_mouse_signal.set_reg {
                old_mouse_signal.set_reg = mouse_signal.set_reg.clone();
                let (keyboard, acti_num) = &mouse_signal.set_reg;
                regs.activate_reg(keyboard.to_string(), *acti_num);
                stops.regs_to_stops(regs);
                let vols = stops.get_amps(express);
                midi_coupler.stops_set_sounds(vols);
            }

            let mut i = 0;
            while i < midi_coupler.input_names.len() {
                if i == 0 {
                    midi_coupler.new_inputs[i] = *midi_great_rcv
                        .lock().unwrap();
                }
                if i == 1 {
                    midi_coupler.new_inputs[i] = *midi_swell_rcv
                        .lock().unwrap();
                }
                if i == 2 {
                    midi_coupler.new_inputs[i] = *midi_pedal_rcv
                        .lock().unwrap();
                }
                if midi_coupler.new_inputs[i] != midi_coupler.actual_inputs[i] {
                    midi_coupler.midi_polls[i] = true;
                    midi_coupler.actual_inputs[i] = midi_coupler.new_inputs[i];
                } else {
                    midi_coupler.midi_polls[i] = false;
                }
                if midi_coupler.midi_polls[i] {
                    let actual_inputs = midi_coupler.actual_inputs;
                    let actual_input = actual_inputs[i];
                    println!("{:?}", actual_input);
                    if actual_input[1] == 175 + midi_channels[i]
                        && actual_input[2] == 7 {
                            express = actual_input[3];
                            let vols = stops.get_amps(express);
                            thread::spawn (move || {
                                midi_coupler.set_express(vols);
                            });
                        }
                    if actual_input[1] == 143 + midi_channels[i]
                        || actual_input[1] == 127 + midi_channels[i] {
                            midi_coupler.set_keys_down(midi_channels);
                            let mut keys_down = keys_down_snd.lock().unwrap();
                            *keys_down = midi_coupler.keys_down;
                            let vols = stops.get_amps(express);
                            midi_coupler.set_sounds(&sounds.chunks,
                                                    vols);
                        }
                }
                i = i + 1;
            }
        }
    }
}
