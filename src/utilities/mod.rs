//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use std::fs;
use std::path::Path;

pub fn dbfs_to_amp(dbfs: f64) -> f64 {
    (dbfs / 8.65618).exp()
}

pub fn poly_to_array(data_str: &str) -> [f64; 128] {
    let mut working_str = data_str.clone();
    let mut float_array: [f64; 128] = [0.0; 128];
    let mut ac = 0;
    while working_str.len() > 0 {
        let find_tab = working_str.find("	");
        if find_tab != None {
            let tab_pos = find_tab.unwrap();
            let try_get_num = &working_str[..tab_pos];
            if try_get_num != "!" {
                float_array[ac] = try_get_num.parse().unwrap();
                ac = ac + 1;
            }
            working_str = &working_str[tab_pos + 1..];
        } else {
            let last_num = &working_str;
            float_array[ac] = last_num.parse().unwrap();
            ac = ac + 1;
            working_str = "";
        }
    }
    float_array
}

pub fn dist_clean() {
    let has_init: bool = Path::new("data/generated-samples").is_dir();
    if has_init == true {
        fs::remove_dir_all("data/generated-samples").unwrap();
    }
    let is_saved: bool =  Path::new("Registrations.toml").is_file();
    if is_saved {
        fs::remove_file("Registrations.toml").unwrap();
    }
    println!("Distribution clean is completed!");
}
