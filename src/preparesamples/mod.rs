//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use std::fs;
use std::process::Command;

use crate::iceorgandata::{Instruments};

pub fn preparesamples () {
    
    // Set the sample rate and bit depth.
    let fs: &str = "96000";
    let br: &str = "24";

    // Set the A4 hertz.
    let ahz = 440.0;
    let hz_factor = ahz / ((2.0_f64.powf(1.0/12.0)).powf(69.0));
    
    let shell: String;
    let shell_cmd: String;
    if cfg!(target_os = "windows") {
        shell = String::from("cmd");
        shell_cmd = String::from("/C");
    } else {
        shell = String::from("sh");
        shell_cmd = String::from("-c");
    }
    let shell = shell.as_str();
    let shell_cmd = shell_cmd.as_str();
    let samplepath = "data/generated-samples";
    fs::create_dir_all(samplepath).unwrap();


    // Instrument sine waves generation.
    let instruments = Instruments::new();
    let mut inst: usize = 1;
    while inst <= 9 {
        let inst_string = "Instrument".to_string() + inst.to_string().as_str();
        let inst_str = inst_string.as_str();
        let instpath = samplepath.to_owned() + "/" + inst_str;
        let instpath_str = instpath.as_str();
        fs::create_dir_all(instpath.as_str()).unwrap();
        let mut note: usize = 0;
        while note < 128 {
            let note_string = note.to_string();
            let note_str = note_string.as_str();
            println!("Generating sound file Instrument{:?}/sine-{:?}.wav",
                     inst, note);
            let atte = instruments.get_data(inst_str)
                .unwrap().attenuation_db;
            let db_adjust = -atte;
            let db_string = db_adjust.to_string();
            let db_str = db_string.as_str();
            let pan = instruments.get_data(inst_str).unwrap().pan[note];
            let mut pan_l = String::from("1.0");
            let mut pan_r = String::from("1.0");
            if pan <= 0.0 {
                pan_r = (1.0 - (pan/50.0).abs()).to_string();
            } else {
                pan_l = (1.0 - (pan/50.0).abs()).to_string();
            }
            let pan_l = pan_l.as_str();
            let pan_r = pan_r.as_str();
            let tuning_semi_tone = instruments.get_data(inst_str)
                .unwrap().tuning_semitones;
            let tuning_cents = instruments.get_data(inst_str)
                .unwrap().tuning_cents[note];
            let tuning: f64;
            if tuning_cents < 0.0 {
                tuning = tuning_semi_tone - (tuning_cents / 100.0);
            } else {
                tuning = tuning_semi_tone + (tuning_cents / 100.0);
            }
            let semi_tone = note as f64  + tuning;
            let tremolo_freq = instruments.get_data(inst_str)
                .unwrap().vib_lfo_freq_hz
                .to_string();
            let tremolo_freq = tremolo_freq.as_str();
            let tremolo_pitch = instruments.get_data(inst_str)
                .unwrap().vib_lfo_pitch_c
                .abs()
                .to_string();
            let tremolo_pitch = tremolo_pitch.as_str();
            let reverb = instruments.get_data(inst_str)
                .unwrap().reverb.to_string();
            let reverb = reverb.as_str();
            let hertz = (hz_factor * (2.0_f64.powf(1.0/12.0)).powf(semi_tone))
                .to_string();
            let hertz = hertz.as_str();
            let sec = "1.0";
            
            // Start generating sine waves below:
            let sox_cmd_string = String::from("sox -V -r ")
                + fs + " -n -b " + br + " -c 1 " + instpath_str + "/sine-l-"
                + note_str + ".wav synth " + sec + " sin " + hertz + " vol "
                + db_str + "dB vol " + pan_l + " amplitude";
            let sox_cmd = sox_cmd_string.as_str();
            let _run_sox_cmd = Command::new(shell)
                .arg(shell_cmd)
                .arg(sox_cmd)
                .output()
                .expect("sox command failed to start");
            let sox_cmd_string = String::from("sox -V -r ")
                + fs + " -n -b " + br + " -c 1 " + instpath_str + "/sine-r-"
                + note_str + ".wav synth " + sec + " sin " + hertz + " vol "
                + db_str + "dB vol " + pan_r + " amplitude";
            let sox_cmd = sox_cmd_string.as_str();
            let _run_sox_cmd = Command::new(shell)
                .arg(shell_cmd)
                .arg(sox_cmd)
                .output()
                .expect("sox command failed to start");
            let sox_cmd_string: String;
            if tremolo_pitch != "0" {
                sox_cmd_string = String::from("sox ")
                    + instpath_str + "/sine-l-" + note_str + ".wav "
                    + instpath_str + "/sine-r-" + note_str + ".wav "
                    + "-c 2 --combine merge "
                    + instpath_str + "/sine-f-" + note_str + ".wav "
                    + "tremolo " + tremolo_freq + " " + tremolo_pitch
                    + " reverb " + reverb + " fade t 0 -0 0.3";
            } else {
                sox_cmd_string = String::from("sox ")
                    + instpath_str + "/sine-l-" + note_str + ".wav "
                    + instpath_str + "/sine-r-" + note_str + ".wav "
                    + "-c 2 --combine merge "
                    + instpath_str + "/sine-f-" + note_str + ".wav "
                    + "reverb " + reverb + " fade t 0 -0 0.3";
            }
            let sox_cmd = sox_cmd_string.as_str();
            let _run_sox_cmd = Command::new(shell)
                .arg(shell_cmd)
                .arg(sox_cmd)
                .output()
                .expect("sox command failed to start");
            let rm_sine_l_path = instpath_str.to_string()
                + "/sine-l-" + note_str + ".wav";
            let rm_sine_r_path = instpath_str.to_string()
                + "/sine-r-" + note_str + ".wav";
            fs::remove_file(rm_sine_l_path.as_str()).unwrap();
            fs::remove_file(rm_sine_r_path.as_str()).unwrap();
            let pad_sec_string = (sec.parse::<f64>().unwrap() - 0.6)
                .to_string();
            let pad_sec = pad_sec_string.as_str();
            let sox_cmd_string = String::from("sox -v -1.0 ")
                + instpath_str + "/sine-f-" + note_str + ".wav "
                + instpath_str + "/sine-b-" + note_str + ".wav "
                + "reverse fade t 0.3 -0 0 pad " + pad_sec + " 0";
            let sox_cmd = sox_cmd_string.as_str();
            let _run_sox_cmd = Command::new(shell)
                .arg(shell_cmd)
                .arg(sox_cmd)
                .output()
                .expect("sox command failed to start");
            let sox_cmd_string = String::from("sox -m ")
                + "-v 1.0 " + instpath_str + "/sine-f-" + note_str + ".wav "
                + "-v 1.0 " + instpath_str + "/sine-b-" + note_str + ".wav "
                + instpath_str + "/sine-" + note_str + ".wav";
            let sox_cmd = sox_cmd_string.as_str();
            let _run_sox_cmd = Command::new(shell)
                .arg(shell_cmd)
                .arg(sox_cmd)
                .spawn()
                .expect("sox command failed to start");            
            note = note + 1;
        }
        inst = inst + 1;
    }

    // The codes below is to manipulate the effect samples.

    let generated_effect_path = "data/generated-samples/effects";
    fs::create_dir_all(generated_effect_path).unwrap();

    // Manipulate the Noise Breath effect.
    let atte = 50.0;
    let db_adjust = -(atte);
    let db_string = db_adjust.to_string();
    let db_str = db_string.as_str();
    let src = "data/effectsamples/noise_breath.wav";
    let dst = generated_effect_path.to_string() + "/noise_breath-use.wav";
    let dst = dst.as_str();
    let start_dst = generated_effect_path.to_string()
        + "/noise_breath-start-use.wav";
    let start_dst = start_dst.as_str();
    let loop_dst = generated_effect_path.to_string()
        + "/noise_breath-loop-use.wav";
    let loop_dst = loop_dst.as_str();
    println!("Generating: {}", start_dst);
    println!("Generating: {}", loop_dst);
    let sox_cmd_string = String::from("sox ") + src + " -c 2 -r "
        + fs + " -b " + br + " " + dst + " vol " + db_str + "dB pitch 100";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .output()
        .expect("sox command failed to start");
    let sox_cmd_string = String::from("sox ") + dst + " " + start_dst
        + " trim 0 1.43865216";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .output()
        .expect("sox command failed to start");
    let sox_cmd_string = String::from("sox ") + dst + " " + loop_dst
        + " trim 1.43865216 1.4";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .output()
        .expect("sox command failed to start");
    fs::remove_file(dst).unwrap();

    // Manipulate the Chiff effect.
    let atte = 42.0;
    let db_adjust = -atte;
    let db_string = db_adjust.to_string();
    let db_str = db_string.as_str();
    let src = "data/effectsamples/Chiff.wav";
    let dst = generated_effect_path.to_string() + "/Chiff-use.wav";
    let dst = dst.as_str();
    println!("Generating: {}", dst);
    let sox_cmd_string = String::from("sox ") + src + " -c 2 -r " + fs
        + " -b " + br
        + " " + dst + " vol " + db_str + "dB fade t 0 -0 5.0";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .spawn()
        .expect("sox command failed to start");
    
    // Manipulate the hall effect.
    let atte = 12.0;
    let db_adjust = -atte;
    let db_string = db_adjust.to_string();
    let db_str = db_string.as_str();
    let scale_tuning = 25.0;
    let pitch_inp = 100.0 * (scale_tuning / 100.0);
    let src = "data/effectsamples/hall.wav";
    let tmp_dst_l = generated_effect_path.to_string() + "/hall-l.wav";
    let tmp_dst_r = generated_effect_path.to_string() + "/hall-r.wav";
    let tmp_dst_l = tmp_dst_l.as_str();
    let tmp_dst_r = tmp_dst_r.as_str();
    let sox_cmd_string = String::from("sox ") + src + " -r " + fs
        + " -b " + br + " " + tmp_dst_l + " vol " + db_str + "dB pitch -1200"
        + " trim 0 1.353380";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .output()
        .expect("sox command failed to start");
    let sox_cmd_string = String::from("sox ") + src + " -r " + fs
        + " -b " + br + " " + tmp_dst_r + " vol " + db_str + "dB pitch -600"
        +  " trim 0 1.353380";
    let sox_cmd = sox_cmd_string.as_str();
    let _run_sox_cmd = Command::new(shell)
        .arg(shell_cmd)
        .arg(sox_cmd)
        .output()
        .expect("sox command failed to start");
    let generated_halls_path = generated_effect_path.to_string() + "/halls";
    let generated_halls_path = generated_halls_path.as_str();
    fs::create_dir_all(generated_halls_path).unwrap();
    let mut note = 0;
    while note <= 127 {
        let note_delta = note as f64 - 60.0;
        let pitch = note_delta * pitch_inp;
        let pitch_string = pitch.to_string();
        let pitch_str = pitch_string.as_str();
        let note_string = note.to_string();
        let note_str = note_string.as_str();
        let sox_cmd_string = String::from("sox ")
            + tmp_dst_l + " " + tmp_dst_r + " "
            + "-c 2 --combine merge "
            + generated_halls_path + "/hall-" + note_str + ".wav "
            + "pitch " + pitch_str;
        let sox_cmd = sox_cmd_string.as_str();
        let _run_sox_cmd = Command::new(shell)
            .arg(shell_cmd)
            .arg(sox_cmd)
            .output()
            .expect("sox command failed to start");
        println!("Generating {}/hall-{}.wav", generated_halls_path, note_str);
        note = note + 1;
    }
    fs::remove_file(tmp_dst_l).unwrap();
    fs::remove_file(tmp_dst_r).unwrap();
}
