//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use sdl2;
use sdl2::image::{self, LoadTexture, InitFlag};
use sdl2::rect::{Rect, Point};
use sdl2::render::{Texture};
use sdl2::keyboard::Keycode;
use sdl2::mouse::MouseButton;
use sdl2::event::Event;
use std::sync::{Arc, Mutex};
use std::path::Path;
use crate::iceorgandata::{Stops, Registrations};
use crate::soundcontrol::{MidiCouplerControl};

#[derive(Debug, Clone, PartialEq)]
pub struct MouseSignal {
    pub set_stop: (String, String, bool),
    pub set_coupler: (String, bool),
    pub set_reg: (String, usize),
}

pub struct StopButton<'a> {
    pub keyboard: String,
    pub name: String,
    pub rect_in: Option<Rect>,
    pub rect_out: Option<Rect>,
    pub file: String,
    pub shafile: String,
    pub texture: Option<Texture<'a>>,
    pub shatexture: Option<Texture<'a>>,
}

pub struct RegButton<'a> {
    pub keyboard: String,
    pub rect_in: Option<Rect>,
    pub rect_out: Option<Rect>,
    pub file: String,
    pub shafile: String,
    pub texture: Option<Texture<'a>>,
    pub shatexture: Option<Texture<'a>>,
}

pub struct Buttons<'a> {
    pub stops: Vec<Vec<StopButton<'a>>>,
    pub regs: Vec<Vec<RegButton<'a>>>,
}

impl Buttons<'_> {
    pub fn new(stops: Stops) -> Buttons {
        let mut stop_buttons: Vec<Vec<StopButton>> = Vec::with_capacity(4);

        // Generate Great manual's buttons.
        let great = stops.great;
        stop_buttons.push(Vec::with_capacity(14));
        let mut i = 0;
        while i < great.len() {
            let keyboard_str = "Great";
            let name = great[i].name;
            let path = "data/images/stops".to_string()
                + "/" + keyboard_str + "/" + name;
            let filename = path.clone() + ".png";
            let shafilename = path.clone() + "-sha.png";
            let size_sq: u32 = 110;
            let pos_x: i32 = (i as i32 % 3) * (size_sq as i32 + 23)
                +  1340 + 22;
            let pos_y: i32 = (i as i32 - (i as i32 % 3)) / 3
                * (size_sq as i32 + 23) + 60;
            
            stop_buttons[0].push(
                StopButton {
                    keyboard: keyboard_str.to_string(),
                    name: name.to_string(),
                    file: filename,
                    shafile: shafilename,
                    rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                    rect_out: Some(Rect::new(pos_x - 10,
                                             pos_y - 10,
                                             size_sq + 20,
                                             size_sq + 20)),
                    texture: None,
                    shatexture: None,
                }
            );
            i = i + 1;
        }
        
        // Generate Swell manual's buttons.        
        let swell = stops.swell;
        stop_buttons.push(Vec::with_capacity(11));
        let mut i = 0;
        while i < swell.len() {
            let keyboard_str = "Swell";
            let name = swell[i].name;
            let path = "data/images/stops".to_string()
                + "/" + keyboard_str + "/" + name;
            let filename = path.clone() + ".png";
            let shafilename = path.clone() + "-sha.png";
            let size_sq: u32 = 110;
            let pos_x: i32 = (i as i32 % 3) * (size_sq as i32 + 23)
                +  0 + 22;
            let pos_y: i32 = (i as i32 - (i as i32 % 3)) / 3
                * (size_sq as i32+ 23) + 60;
            
            stop_buttons[1].push(
                StopButton {
                    keyboard: keyboard_str.to_string(),
                    name: name.to_string(),
                    file: filename,
                    shafile: shafilename,
                    rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                    rect_out: Some(Rect::new(pos_x - 10,
                                             pos_y - 10,
                                             size_sq + 20,
                                             size_sq + 20)),
                    texture: None,
                    shatexture: None,
                }
            );
            i = i + 1;
        }
        
        // Generate Pedal buttons.        
        let pedal = stops.pedal;
        stop_buttons.push(Vec::with_capacity(10));
        let mut i = 0;
        while i < pedal.len() {
            let keyboard_str = "Pedal";
            let name = pedal[i].name;
            let path = "data/images/stops".to_string()
                + "/" + keyboard_str + "/" + name;
            let filename = path.clone() + ".png";
            let shafilename = path.clone() + "-sha.png";
            let size_sq: u32 = 110;
            let pos_x: i32 = (i as i32 % 4) * (size_sq as i32 + 23)
                +  0 + 22;
            let pos_y: i32 = (i as i32 - (i as i32 % 4)) / 3
                * (size_sq as i32 + 23) + 592;
            
            stop_buttons[2].push(
                StopButton {
                    keyboard: keyboard_str.to_string(),
                    name: name.to_string(),
                    file: filename,
                    shafile: shafilename,
                    rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                    rect_out: Some(Rect::new(pos_x - 10,
                                             pos_y - 10,
                                             size_sq + 20,
                                             size_sq + 20)),
                    texture: None,
                    shatexture: None,
                }
            );
            i = i + 1;
        }
        
        // Generate coupler buttons.
        stop_buttons.push(Vec::with_capacity(3));
        let size_sq: u32 = 110;
        let pos_x: i32 = (0 as i32 % 4) * (size_sq as i32 + 23)
            +  1340 + 22;
        let pos_y: i32 = (0 as i32 - (0 as i32 % 4)) / 3
            * (size_sq as i32 + 23) + 856;
        stop_buttons[3].push(
            StopButton {
                keyboard: "coupler".to_string(),
                name: "Swell-Great".to_string(),
                file: "data/images/stops/couplers/swell-great.png".to_string(),
                shafile: "data/images/stops/couplers/swell-great-sha.png".
                    to_string(),
                rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                rect_out: Some(Rect::new(pos_x - 10,
                                         pos_y - 10,
                                         size_sq + 20,
                                         size_sq + 20)),
                texture: None,
                shatexture: None,
            }
        );
        let pos_x: i32 = (1 as i32 % 4) * (size_sq as i32 + 23)
            +  1340 + 22;
        let pos_y: i32 = (1 as i32 - (1 as i32 % 4)) / 3
            * (size_sq as i32 + 23) + 856;
        stop_buttons[3].push(
            StopButton {
                keyboard: "coupler".to_string(),
                name: "Swell-Pedal".to_string(),
                file: "data/images/stops/couplers/swell-pedal.png".to_string(),
                shafile: "data/images/stops/couplers/swell-pedal-sha.png".
                    to_string(),
                rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                rect_out: Some(Rect::new(pos_x - 10,
                                         pos_y - 10,
                                         size_sq + 20,
                                         size_sq + 20)),
                texture: None,
                shatexture: None,
            }
        );        
        let pos_x: i32 = (2 as i32 % 4) * (size_sq as i32 + 23)
            +  1340 + 22;
        let pos_y: i32 = (2 as i32 - (2 as i32 % 4)) / 3
            * (size_sq as i32 + 23) + 856;
        stop_buttons[3].push(
            StopButton {
                keyboard: "coupler".to_string(),
                name: "Great-Pedal".to_string(),
                file: "data/images/stops/couplers/great-pedal.png".to_string(),
                shafile: "data/images/stops/couplers/great-pedal-sha.png".
                    to_string(),
                rect_in: Some(Rect::new(pos_x, pos_y, size_sq, size_sq)),
                rect_out: Some(Rect::new(pos_x - 10,
                                         pos_y - 10,
                                         size_sq + 20,
                                         size_sq + 20)),
                texture: None,
                shatexture: None,
            }
        );

        let mut reg_buttons: Vec<Vec<RegButton>> = Vec::with_capacity(3);
        let mut i: usize = 0;
        while i < 3 {
            reg_buttons.push(Vec::with_capacity(10));
            let mut j: usize = 0;
            while j < 10 {
                let keyboard: String;
                if i == 0 {
                    keyboard = "Great".to_string();
                } else if i == 1 {
                    keyboard = "Swell".to_string();
                } else {
                    keyboard = "Pedal".to_string();
                }
                let j_string = j.to_string();
                let j_str = j_string.as_str();
                let filepath = "data/images/registrations/reg-".to_string()
                    + j_str + ".png";
                let shafilepath = "data/images/registrations/reg-".to_string()
                    + j_str + "-sha.png";
                let size_sq: u32 = 50;
                let pos_x: i32 = 420 + 16 + j as i32 * 92;
                let pos_y: i32;
                if i == 0 {
                    pos_y = 80 + 110 + 16 + 60 + 16 + 110 + 16;
                } else if i == 1 {
                    pos_y = 80 + 110 + 16;
                } else {
                    pos_y = 80 + 110 + 16 + 60 + 16 + 110 + 16 + 60 + 16 + 20;
                }
                reg_buttons[i].push(
                    RegButton {
                        keyboard: keyboard,
                        rect_in: Some(Rect::new(pos_x, pos_y,
                                                size_sq,
                                                size_sq)),
                        rect_out: Some(Rect::new(pos_x - 5, pos_y - 5,
                                                 size_sq + 10,
                                                 size_sq + 10)),
                        file: filepath,
                        shafile: shafilepath,
                        texture: None,
                        shatexture: None,
                    }
                );
                j = j + 1
            }
            i = i + 1;
        }
        
        Buttons{
            stops: stop_buttons,
            regs: reg_buttons,
        }
    }
}

pub fn coupler_mod(keys_down_rcved: [[bool; 128]; 3],
                   midi_coupler: MidiCouplerControl)
                   -> [[bool; 128]; 3] {
    let mut keys_down_rcved = keys_down_rcved;
    let swell_great = midi_coupler.get_coupler("Swell-Great").unwrap();
    let swell_pedal = midi_coupler.get_coupler("Swell-Pedal").unwrap();
    let great_pedal = midi_coupler.get_coupler("Swell-Great").unwrap();
    if swell_great {
        let mut i = 0;
        while i < keys_down_rcved[0].len() {
            if keys_down_rcved[1][i] == false {
                keys_down_rcved[1][i] = keys_down_rcved[0][i];
            }
            i = i + 1;
        }
    }
    if swell_pedal {
        let mut i = 0;
        while i < keys_down_rcved[2].len() {
            if keys_down_rcved[1][i] == false {
                keys_down_rcved[1][i] = keys_down_rcved[2][i];
            }
            i = i + 1;
        }
    }
    if great_pedal {
        let mut i = 0;
        while i < keys_down_rcved[2].len() {
            if keys_down_rcved[0][i] == false {
                keys_down_rcved[0][i] = keys_down_rcved[2][i];
            }
            i = i + 1;
        }
    }
    keys_down_rcved
}

pub fn display_window(exit_snd: Arc<Mutex<bool>>,
                      mut stops: Stops,
                      mut regs: Registrations,
                      mut midi_coupler: MidiCouplerControl,
                      mouse_signal_snd: Arc<Mutex<MouseSignal>>,
                      keys_down_rcv: Arc<Mutex<[[bool; 128]; 3]>>,
                      midi_channels_snd: Arc<Mutex<[u64; 3]>>){
    let window_size = (1760, 990);
    
    let (win_x, win_y) = window_size;
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let _image_context = image::init(InitFlag::PNG).unwrap();
    let window = video_subsystem.window("IceOrgan-R", win_x, win_y)
        .build()
        .unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut canvas = window.into_canvas()
        .present_vsync()
        .build().unwrap();
    let texture_creator = canvas.texture_creator();
    let backboard = texture_creator.load_texture(
        "./data/images/wood-backboard.png").unwrap();
    let keyboard = texture_creator.load_texture(
        "./data/images/keyboard-88.png").unwrap();
    let pedalboard = texture_creator.load_texture(
        "./data/images/pedalboard.png").unwrap();
    const KEY_IMAGE_INIT: Option<Texture> = None;
    const KEY_IMAGES_INIT: [Option<Texture>; 128] = [KEY_IMAGE_INIT; 128];
    let mut key_images: [[Option<Texture>; 128]; 3] = [KEY_IMAGES_INIT; 3];
    let mut i = 0;
    while i < key_images.len() {
        let mut j = 0;
        while j < key_images[i].len() {
            let note_string = j.to_string();
            let note_str = note_string.as_str();
            let image_filename_string = "key-".to_string() + note_str + ".png";
            let image_filename_str = image_filename_string.as_str();
            let image_path_string: String;
            if i != 2 {
                image_path_string = "data/images/keys/".to_string()
                    + image_filename_str;
            } else {
                image_path_string = "data/images/pedal-keys/".to_string()
                    + image_filename_str;
            }
            let image_path = image_path_string.as_str();
            if Path::new(image_path).is_file() {
                key_images[i][j] = Some(texture_creator.load_texture(
                    image_path).unwrap());
            } else {
                key_images[i][j] = None;
            }
            j = j + 1;
        }
        i = i + 1;
    }
    let mut buttons = Buttons::new(stops);
    let mut i = 0;
    while i < buttons.stops.len() {
        let mut j = 0;
        while j < buttons.stops[i].len() {
            let buttons_texture = texture_creator.load_texture(
                buttons.stops[i][j].file.clone()
            ).unwrap();
            buttons.stops[i][j].texture = Some(buttons_texture);
            let buttons_shatexture = texture_creator.load_texture(
                buttons.stops[i][j].shafile.clone()
            ).unwrap();
            buttons.stops[i][j].shatexture = Some(buttons_shatexture);
            j = j + 1;
        }
        i = i + 1;
    }
    let mut i = 0;
    while i < buttons.regs.len() {
        let mut j = 0;
        while j < buttons.regs[i].len() {
            let buttons_texture = texture_creator.load_texture(
                buttons.regs[i][j].file.clone()
            ).unwrap();
            buttons.regs[i][j].texture = Some(buttons_texture);
            let buttons_shatexture = texture_creator.load_texture(
                buttons.regs[i][j].shafile.clone()
            ).unwrap();
            buttons.regs[i][j].shatexture = Some(buttons_shatexture);
            j = j + 1;
        }
        i = i + 1;
    }

    let mut channel_sel_mode = false;
    let mut channel_sel_modes = [false, false, false];
    let mut ctrl_down = false;
    let mut input_text = String::new();
    'display_loop: loop {
        let mut mouse_x: i32 = 0;
        let mut mouse_y: i32 = 0;
        let mut mouse_left_down = false;
        let mouse_state = event_pump.mouse_state();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    let mut should_exit = exit_snd.lock().unwrap();
                    *should_exit = true;
                    break 'display_loop;
                },
                Event::MouseButtonDown { mouse_btn: MouseButton::Left, .. } => {
                    mouse_x = mouse_state.x();
                    mouse_y = mouse_state.y();
                    mouse_left_down = true;
                },
                Event::KeyDown { keycode: Some(Keycode::LCtrl), .. } => {
                    ctrl_down = true;
                },
                Event::KeyUp { keycode: Some(Keycode::LCtrl), .. } => {
                    ctrl_down = false;
                },
                Event::KeyDown { keycode: Some(Keycode::C), .. } => {
                    if ctrl_down && channel_sel_mode == false {
                        channel_sel_mode = true;
                        println!("Channel selection mode: {}",
                                 channel_sel_mode);
                        input_text = String::new();
                    } else if ctrl_down {
                        channel_sel_mode = false;
                        println!("Channel selection mode: {}",
                                 channel_sel_mode);
                        input_text = String::new();
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::G), .. } => {
                    if ctrl_down && channel_sel_mode {
                        channel_sel_modes[0] = true;
                        println!("Great channel selection mode: {}",
                                 channel_sel_modes[0]);
                        input_text = String::new();
                        channel_sel_mode = false;
                    } else if ctrl_down {
                        channel_sel_modes = [false, false, false];
                        input_text = String::new();
                        channel_sel_mode = false;
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::S), .. } => {
                    if ctrl_down && channel_sel_mode {
                        channel_sel_modes[1] = true;
                        println!("Swell channel selection mode: {}",
                                 channel_sel_modes[1]);
                        input_text = String::new();
                        channel_sel_mode = false;
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::P), .. } => {
                    if ctrl_down && channel_sel_mode {
                        channel_sel_modes[2] = true;
                        println!("Pedal channel selection mode: {}",
                                 channel_sel_modes[2]);
                        input_text = String::new();
                        channel_sel_mode = false;
                    }
                },
                Event::TextInput {text, ..} => {
                    if channel_sel_mode == false {
                        let mut i = 0;
                        while i < 3 {
                            if channel_sel_modes[i] {
                                input_text = input_text + text.as_str();
                                println!("Channel number input: {}"
                                         , input_text);
                            }
                            i = i + 1;
                        }
                    }
                },
                Event::KeyDown {keycode: Some(Keycode::Return), ..} => {
                    let mut i = 0;
                    while i < 3 {
                        let mut channel_inputs = midi_channels_snd
                            .lock().unwrap();
                        if channel_sel_modes[i] {
                            channel_sel_modes[i] = false;
                            channel_inputs[i] = input_text
                                .parse().unwrap();
                            if i == 0 {
                                println!("Your new Great midi channel: {}",
                                         channel_inputs[i]);
                            } else if i == 1 {
                                println!("Your new Swell midi channel: {}",
                                         channel_inputs[i]);
                            } else if i == 2 {
                                println!("Your new Pedal midi channel: {}",
                                         channel_inputs[i]);
                            }
                            input_text = String::new();
                        }
                        i = i + 1;
                    }
                },
                _ => {}
            }
        }
        
        canvas.clear();
        canvas.copy(&backboard, None, None).unwrap();
        
        let great_rect = Some(Rect::new(420,
                                   80 + 110 + 16 + 60 + 16 ,
                                   920,
                                   110));
        canvas.copy(&keyboard,
                    None,
                    great_rect).unwrap();
        
        let swell_rect = Some(Rect::new(420, 80, 920, 110));
        canvas.copy(&keyboard,
                    None,
                    swell_rect).unwrap();

        let pedal_rect = Some(Rect::new(520, 600, 720, 405));
        canvas.copy(&pedalboard,
                    None,
                    pedal_rect).unwrap();
        
        let mut i = 0;
        while i < buttons.stops.len() {
            let mut j = 0;
            while j < buttons.stops[i].len() {
                let is_out: bool;                
                if i == 0 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.stops[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && stops.great[j].is_out {
                            stops.great[j].is_out = false;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Great".to_string(),
                                stops.great[j].name.to_string(),
                                stops.great[j].is_out
                            );
                            regs.stops_to_regs(stops);
                        } else if on_this_btn {
                            stops.great[j].is_out = true;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Great".to_string(),
                                stops.great[j].name.to_string(),
                                stops.great[j].is_out
                            );                            
                            regs.stops_to_regs(stops);
                        }
                    }
                    
                    
                    is_out = stops.great[j].is_out;
                } else if i == 1 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.stops[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && stops.swell[j].is_out {
                            stops.swell[j].is_out = false;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Swell".to_string(),
                                stops.swell[j].name.to_string(),
                                stops.swell[j].is_out
                            );                            
                            regs.stops_to_regs(stops);
                        } else if on_this_btn {
                            stops.swell[j].is_out = true;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Swell".to_string(),
                                stops.swell[j].name.to_string(),
                                stops.swell[j].is_out
                            );                            
                            regs.stops_to_regs(stops);
                        }
                    }
                    is_out = stops.swell[j].is_out;
                } else if i == 2 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.stops[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && stops.pedal[j].is_out {
                            stops.pedal[j].is_out = false;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Pedal".to_string(),
                                stops.pedal[j].name.to_string(),
                                stops.pedal[j].is_out
                            );                            
                            regs.stops_to_regs(stops);
                        } else if on_this_btn {
                            stops.pedal[j].is_out = true;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_stop = (
                                "Pedal".to_string(),
                                stops.pedal[j].name.to_string(),
                                stops.pedal[j].is_out
                            );
                            regs.stops_to_regs(stops);
                        }
                    }
                    is_out = stops.pedal[j].is_out;
                } else {
                    if j == 0 {
                        let check_is_out = midi_coupler
                            .get_coupler("Swell-Great").unwrap();
                        if mouse_left_down {
                            let on_this_btn: bool;
                            if buttons.stops[i][j].rect_out
                                .unwrap().contains_point(
                                    Point::new(mouse_x, mouse_y))
                            {
                                on_this_btn = true;
                            } else {
                                on_this_btn = false;
                            }
                            if on_this_btn && check_is_out {
                                midi_coupler.set_coupler("Swell-Great", false);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Swell-Great".to_string(),
                                    midi_coupler.get_coupler("Swell-Great")
                                        .unwrap()
                                );
                                regs.couplers_to_regs(midi_coupler);
                            } else if on_this_btn {
                                midi_coupler.set_coupler("Swell-Great", true);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Swell-Great".to_string(),
                                    midi_coupler.get_coupler("Swell-Great")
                                        .unwrap()
                                );
                                regs.couplers_to_regs(midi_coupler);
                            }
                        }
                        is_out = midi_coupler
                            .get_coupler("Swell-Great").unwrap();
                        } else if j == 1 {
                        let check_is_out = midi_coupler
                            .get_coupler("Swell-Pedal").unwrap();
                        if mouse_left_down {
                            let on_this_btn: bool;
                            if buttons.stops[i][j].rect_out
                                .unwrap().contains_point(
                                    Point::new(mouse_x, mouse_y))
                            {
                                on_this_btn = true;

                            } else {
                                on_this_btn = false;
                            }
                            if on_this_btn && check_is_out {
                                midi_coupler.set_coupler("Swell-Pedal", false);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Swell-Pedal".to_string(),
                                    midi_coupler.get_coupler("Swell-Pedal")
                                        .unwrap()
                                );                            
                                regs.couplers_to_regs(midi_coupler);
                            } else if on_this_btn {
                                midi_coupler.set_coupler("Swell-Pedal", true);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Swell-Pedal".to_string(),
                                    midi_coupler.get_coupler("Swell-Pedal")
                                        .unwrap()
                                );
                                regs.couplers_to_regs(midi_coupler);
                            }
                        }
                        is_out = midi_coupler
                            .get_coupler("Swell-Pedal").unwrap();
                    } else {
                        let check_is_out = midi_coupler
                            .get_coupler("Great-Pedal").unwrap();
                        if mouse_left_down {
                            let on_this_btn: bool;
                            if buttons.stops[i][j].rect_out
                                .unwrap().contains_point(
                                    Point::new(mouse_x, mouse_y))
                            {
                                on_this_btn = true;
                            } else {
                                on_this_btn = false;
                            }
                            if on_this_btn && check_is_out {
                                midi_coupler.set_coupler("Great-Pedal", false);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Great-Pedal".to_string(),
                                    midi_coupler.get_coupler("Great-Pedal")
                                        .unwrap()
                                );                            
                                regs.couplers_to_regs(midi_coupler);
                            } else if on_this_btn {
                                midi_coupler.set_coupler("Great-Pedal", true);
                                let mut mouse_signal = mouse_signal_snd.lock()
                                    .unwrap();
                                mouse_signal.set_coupler = (
                                    "Great-Pedal".to_string(),
                                    midi_coupler.get_coupler("Great-Pedal")
                                        .unwrap()
                                );
                                regs.couplers_to_regs(midi_coupler);
                            }
                        }
                        is_out = midi_coupler
                            .get_coupler("Great-Pedal").unwrap();
                    }
                }
                if is_out {
                    canvas.copy(&buttons.stops[i][j].shatexture.as_ref()
                                .unwrap(),
                                None,
                                buttons.stops[i][j].rect_out).unwrap();
                } else {
                    canvas.copy(&buttons.stops[i][j].texture.as_ref()
                                .unwrap(),
                                None,
                                buttons.stops[i][j].rect_in).unwrap();
                }
                j = j + 1;
            }
            i = i + 1;
        }
        let mut i = 0;
        while i < buttons.regs.len() {
            let mut j = 0;
            while j < buttons.regs[i].len() {
                if i == 0 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.regs[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && regs.working_reg_great != j {
                            regs.working_reg_great = j;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_reg = ("Great".to_string(), j);
                            stops.regs_to_stops(regs);
                        }
                    }
                    let activated_reg = regs.working_reg_great;
                    if j == activated_reg {
                        canvas.copy(&buttons.regs[i][j].texture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_in)
                            .unwrap();
                    } else {
                        canvas.copy(&buttons.regs[i][j].shatexture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_out)
                            .unwrap();
                    }
                }
                if i == 1 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.regs[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && regs.working_reg_swell != j {
                            regs.working_reg_swell = j;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_reg = ("Swell".to_string(), j);
                            stops.regs_to_stops(regs);
                        }
                    }
                    let activated_reg = regs.working_reg_swell;
                    if j == activated_reg {
                        canvas.copy(&buttons.regs[i][j].texture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_in)
                            .unwrap();
                    } else {
                        canvas.copy(&buttons.regs[i][j].shatexture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_out)
                            .unwrap();
                    }
                }
                if i == 2 {
                    if mouse_left_down {
                        let on_this_btn: bool;
                        if buttons.regs[i][j].rect_out
                            .unwrap().contains_point(
                                Point::new(mouse_x, mouse_y))
                        {
                            on_this_btn = true;
                        } else {
                            on_this_btn = false;
                        }
                        if on_this_btn && regs.working_reg_pedal != j {
                            regs.working_reg_pedal = j;
                            let mut mouse_signal = mouse_signal_snd.lock()
                                .unwrap();
                            mouse_signal.set_reg = ("Pedal".to_string(), j);
                            stops.regs_to_stops(regs);
                        }
                    }
                    let activated_reg = regs.working_reg_pedal;
                    if j == activated_reg {
                        canvas.copy(&buttons.regs[i][j].texture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_in)
                            .unwrap();
                    } else {
                        canvas.copy(&buttons.regs[i][j].shatexture.as_ref()
                                    .unwrap(),
                                    None,
                                    buttons.regs[i][j].rect_out)
                            .unwrap();
                    }
                }
                j = j + 1;
            }
            i = i + 1;
        }

        let mut keys_down_rcved = *keys_down_rcv.lock().unwrap();
        keys_down_rcved = coupler_mod(keys_down_rcved, midi_coupler);
        let mut i = 0;
        while i < key_images.len() {
            let mut j = 0;
            while j < key_images[i].len() {
                if keys_down_rcved[i][j] == true
                    && key_images[i][j].is_some()
                {
                    key_images[i][j].as_mut().unwrap().set_alpha_mod(200);
                } else if key_images[i][j].is_some() {
                    key_images[i][j].as_mut().unwrap().set_alpha_mod(0);
                }
                if key_images[i][j].is_some() {
                    if i == 0 {
                        canvas.copy(&key_images[i][j].as_mut().unwrap(),
                                    None,
                                    great_rect).unwrap();
                    } else if i == 1 {
                        canvas.copy(&key_images[i][j].as_mut().unwrap(),
                                    None,
                                    swell_rect).unwrap();
                    } else {
                        canvas.copy(&key_images[i][j].as_mut().unwrap(),
                                    None,
                                    pedal_rect).unwrap();
                    }
                }
                j = j + 1;
            }
            i = i + 1;
        }
        canvas.present();
    }
}
