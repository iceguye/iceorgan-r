# IceOrgan-R

## Introduction

This program is a rewrite of the old Python IceOrgan in Rust
programming language. The goal of this rewrite is to get the same
usability of the old IceOrgan, but it will be more stable and
efficient by taking the benefits of Rust.

IceOrgan-R is a **real-time** implementation of the hedOrgan
pipe-organ soundfont written by Ziya Mete Demircan. However, it does
not directly load the soundfont's sf2 file. It works standalone.

Ziya Mete Demircan used his owned special programs to generate a very
special but really good soundfont for simulating a pipe
organ. However, due to the limitation of the sf2 soundfont, it can be
just used for note writing software such as Musescore. It can hardly
be used for real-time. This is why I wrote this program, IceOrgan-R,
to implement it for real-time.

The original sf2 can be found here if you need it:
<https://www.hedsound.com/2019/10/hedorgan-pipe-organ-soundfont.html>

Same as the actual pipe organ which Ziya simulated, IceOrgan-R also
includes following stops:

```
+--+----------------------+--+-----------------------+--+---------------------+
| #| [MI] (Great)         | #|  [MII] (Swell)        | #| [P] (Pedal)         |
|--+----------------------+--+-----------------------+--+---------------------+
|01| G 1-Trumpet8'        |01|  S 1-Lieblgedekt 16'  |01| P 1-Posaune16'      |
|02| G 2-Mixture5         |02|  S 2-GeigenPrinzipl16 |02| P 2-PrinzipalBass16 |
|03| G 3-Cornet2&4'       |03|  S 3-Gedekt 8'        |03| P 3-BassViolin16'   |
|04| G 4-Raushquinte2_2/3 |04|  S 4-Concert Flute 8  |04| P 4-SubBass16'      |
|05| G 5-Piccolo2'        |05|  S 5-Gemshorn 8'      |05| P 5-EchoBass16'     |
|06| G 6-Octave4'         |06|  S 6-Chalumeau 8'     |06| P 6-Quintbass10_2/3 |
|07| G 7-HohlFlute4'      |07|  S 7-Aeoline 8'       |07| P 7-OctaveBass8'    |
|08| G 8-HarmonicFlute8'  |08|  S 8-VoixCeleste 8'   |08| P 8-Violoncello8'   |
|09| G 9-Rohrflote8'      |09|  S 9-Fugara 4'        |09| P 9-BassFlute8'     |
|10| G A-Salicional8'     |10|  S A-Travesflute 4'   |10| P A-OctaveBass4'    |
|11| G B-Gamba8'          |11|  S B-Progressio 2&4'  +------------------------+
|12| G C-Prinzipal8'      +--------------------------+
|13| G D-Bourdon16'       |
|14| G E-Prinzipal16'     |
+--+----------------------+
```

## Installation

Dependencies:

** You do NOT need to download the soundfont from the link above,
because this program does NOT rely on it. **

sox >= 14.4.2.0

SDL2 >= 2.0.8

SDL2_mixer >= 2.0.4

SDL2_image >= 2.0.5

Install Rust toolchain first (https://rustup.rs/), then:

For the first time build:

    git clone https://gitlab.com/iceguye/iceorgan-r
    cd iceorgan-r
    cargo build --release

For updating to the latest version:

    cd iceorgan-r
    git pull
    cargo build --release

## Usage

Plug 3 USB midi keyboards to your computer.

If you don't have 3 midi keyboards, you can use some midi keyboard
simulators instead, such as VMPK.

Then run:

    cargo run --release

To change the midi channel for each keyboard:

We use some sort of Emacs combination keys style. This must be done
when the window of IceOrgan-R is active; this cannot be done in your
terminal, or the program may quit.

For Great manual:

    ctrl+c ctrl+g MIDI_CHANNEL_NUMBER Return

For Swell manual:

    ctrl+c ctrl+s MIDI_CHANNEL_NUMBER Return

For Pedal:

    ctrl+c ctrl+p MIDI_CHANNEL_NUMBER Return



## Authors

- IceGuye

    -- Main developer

## Copyright

Copyright © 2022 IceGuye

Unless otherwise noted, all files in this project, including but NOT
limited to source codes and art contents, are free software: you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, version 3
of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.