Copyright © 2022 Hao.

This document is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

    "#ebeb3b", // C
    
    "#933beb", // C#/Db
    
    "#3beb3b", // D

    "#eb3b93", // D#/Eb
               
    "#3bebeb", // E

    "#eb933b", // F

    "#3b3beb", // F#/Gb

    "#93eb3b", // G

    "#eb3beb", // G#/Ab

    "#3beb93", // A

    "#eb3b3b", // A#/Bb

    "#3b93eb" // B